import {
  SingleThing,
  WebThingServer,
} from 'webthing';
import WebThingHTTPTester from 'webthing-http-tester';

import WebThing from '../src/webthing.js';


const PORT = 8000;

describe('test', () => {
  const thing = new WebThing();
  let server = new WebThingServer(new SingleThing(thing), PORT);
  let tester = new WebThingHTTPTester({port: PORT});

  beforeAll(() => server.start());
  beforeEach(async() => {
    await tester.setProperty('a', 0);
    await tester.setProperty('b', 0);
  });
  afterAll(() => server.stop());
  test('matches snapshot', async () => {
    const data = await tester.getResponse('/');
    data.links = '##redacted##';
    expect(data).toMatchSnapshot();
  });
  test('actions matches snapshot', async () => {
    const data = await tester.getResponse('/actions');
    expect(data).toMatchSnapshot();
  });
  test('events matches snapshot', async () => {
    const data = await tester.getResponse('/events');
    expect(data).toMatchSnapshot();
  });
  test('properties matches snapshot', async () => {
    const data = await tester.getResponse('/properties');
    expect(data).toMatchSnapshot();
  });
  test('0 + 0 = 0', async () => {
    await tester.setProperty('a', 0);
    await tester.setProperty('b', 0);
    expect(await tester.getProperty('y')).toBe(0);
  });
  test('76 + 34 = 110', async () => {
    await tester.setProperty('a', 76);
    await tester.setProperty('b', 34);
    expect(await tester.getProperty('y')).toBe(110);
  });
  test('8632942 + (-29332844) = -20699902', async () => {
    await tester.setProperty('a', 8632942);
    await tester.setProperty('b', -29332844);
    expect(await tester.getProperty('y')).toBe(-20699902);
  });
  test('multiple calculations in sequence', async () => {
    await tester.setProperty('a', 5);
    await tester.setProperty('b', 0);
    expect(await tester.getProperty('y')).toBe(5);
    await tester.setProperty('b', -10);
    expect(await tester.getProperty('y')).toBe(-5);
    await tester.setProperty('a', 8632942);
    expect(await tester.getProperty('y')).toBe(8632932);
    await tester.setProperty('b', 1000000);
    expect(await tester.getProperty('y')).toBe(9632942);
  });
  test('the result stays unchanged', async () => {
    expect.assertions(1);
    return tester.setProperty('y', 100).catch(e => expect(e).toMatch('Server answered HTTP error 400: BAD REQUEST'));
  });
});
