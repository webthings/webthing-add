## 0.3.1 (2024-02-16)

- Tested with NodeJS 20
- No code changes

## 0.3.0 (2023-05-24)

- Things now require an `id`. I've set the default to `urn:dev:ops:math-add`.
- Things now use `title` rather than `name` for the thing and all properties.
- Now a JavaScript module.

## 0.2.2 (2019-01-20)

- Update dependency webthing to 0.11.0

## 0.2.1 (2018-09-22)

- Persist state between restarts
- Update webthing-node to 0.8.0
- Implement readOnly flag
