import { readFile, writeFile } from 'fs/promises';
import {
  SingleThing,
  WebThingServer,
} from 'webthing';
import WebThing from './webthing.js';

const config = {
  port: process.env.PORT || 8000,
  stateFile: process.env.STATE_FILE || 'state.json',
};

function runServer(state) {
  const thing = new WebThing({}, state);
  // The server terminates itself on SIGINT.
  const server = new WebThingServer(new SingleThing(thing), config.port);

  process.on('SIGINT', () => {
    const stateJson = JSON.stringify(thing.saveState());
    console.log(thing.saveState(), stateJson);
    writeFile(config.stateFile, stateJson, 'utf8').then(() => {
      console.log('Stored the state');
    }).catch((err) => {
      console.error(err);
      console.error('Failed to store state');
    }).finally(() => {
      process.exit();
    });
  });

  server.start();
}

let state;
readFile(config.stateFile, 'utf8').then((data) => {
  state = JSON.parse(data);
}).catch((err) => {
  console.warn(err);
  console.warn('Was not able to load a saved state.');
}).finally(() => {
  runServer(state);
});
