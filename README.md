# WebThing: Add
This is a WebThing to perform an addition of two numbers. Use it to add numbers in gateways which do not support adding two outputs.

![A preview image of the WebThing Add in the Webthings Gateway](preview.png)

## Explanation
`A + B = Y`

Y is the sum of the modifiable properties A and B.

Property | Internal name | Type | Modifiable? |  Description
---|---|---|---|---
**A** | `a` | number | yes | First summand
**B** | `b` | number | yes | Second summand
**Y** | `y` | number | no | The sum of A and B

## Installation
Download the repository into a folder and install the dependencies. You may do this by running:

```
# Download the repository via git
git clone https://gitlab.com/webthings/webthing-add.git

# Install project dependencies
npm ci
```

## Run
```
# Specify a port in Bash (Linux and Mac)
#export PORT=8000
# Specify a port in Batch (Windows)
#SET /A PORT=8000

npm start
```

This starts a web server on your machine. By default it starts on TCP port 8000. Determine your system’s local IP address and add the WebThing to your gateway.

### Trouble shooting
#### It fails to start with `Error: listen EADDRINUSE :::8000`
You already have an instance of this project or another application running on the mentioned TCP port. On a computer a port cannot be assigned to two servers.  
On Linux you can identify open TCP ports and the owner process with the command `netstat -lnpt`.

#### The server is not reachable from another computer
Ensure that the computers are on the same network. Test their connectivity with the `ping` command. If they are connected, the computer running the server might block the TCP port with its firewall.
